const express = require('express');
const Album = require('../models/Album');

const router = express.Router();

router.get('/',(req,res)=>{
    if (req.query.artist) {
        Album.find({artist : req.query.artist})
            .then(albums => res.send(albums))
            .catch(()=>res.sendStatus(500))
    } else{
        Album.find()
            .then(albums => res.send(albums))
            .catch(()=> res.sendStatus(500));
    }
});
router.post('/',(req,res)=>{
    const album = new Album(req.body);
   album.save()
       .then(result => res.send(result))
       .catch(error => res.status(400).send(error));
});

module.exports = router;