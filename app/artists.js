const express = require('express');
const Artist = require('../models/Artist');

const router = express.Router();

router.get('/',(req,res)=>{
    Artist.find()
        .then(artists => res.send(artists))
        .catch(()=> res.sendStatus(500));
});

router.post('/',(req,res)=>{
    const artist = new Artist(req.body);
    artist.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error));
});

module.exports = router;