const express = require('express');
const User = require ('../models/User');
const TrackHistory = require('../models/TrackHistory');

const router = express.Router();

router.post('/',async (req,res)=>{
    const token = req.get('Token');
    const user = await User.findOne({token});
    console.log(user);
    if (!user){
        return res.status(401).send({error:'Unauthorized user'})
    }
    const track_history = new TrackHistory({
        track: req.body.track,
        user: user._id,
        datetime: new Date().toISOString()
    });
    console.log(track_history);
    track_history.save()
        .then((result)=>res.send(result))
        .catch(()=> res.sendStatus(400));
    return router;
});

module.exports = router;