const mongoose = require('mongoose');

const Artist =require('./models/Artist');
const Album =require('./models/Album');
const Track =require('./models/Track');
const config = require('./config');

const run =async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for(let collection of collections){
        await collection.drop();
    }

    const [artist, artist2,artist3] = await Artist.create(
        {
            name: "Freddy Mercury",
            photo: null,
            info: "Queen front-man"
        },
        {
            name: "Shakira",
            photo: null,
            info: 'Woman singer with strong voice'
        },
        {
            name: "Gazette",
            photo: null,
            info: "Japan J-rock group"
        }
    );

    const [album, album2,album3] = await Album.create(
        {
            name:'Bohemian Rhapsody',
            artist: artist._id,
            year: '1975',
            cover_img: null

        },
        {
            name: "Zootopia OST",
            artist: artist2._id,
            year: '2015',
            cover_img: null
        },
        {
            name: "Black Butler OST",
            artist: artist3._id,
            year: '2014',
            cover_img: null
        }
    );

    const [track, track2,track3,track4] = await Track.create(
        {
            name:'Shiver',
            album: album3._id,
            long: 5,

        },
        {
            name:'Bohemian Rhapsody',
            album: album._id,
            long: 6,

        },
        {
            name:'Try Everything',
            album: album2._id,
            long: 4,

        },
        {
            name: 'I\'m in love with my car',
            album: album._id,
            long: 3
        }
    );
    return connection.close();

};


run().catch(error =>{
    console.log('Something wrong happened ...' ,error);
});